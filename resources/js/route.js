import Vue from "vue";
import Router from "vue-router";
import Home from "../../app/Modules/auth/resources/js/views/Home";
import Login from "../../app/Modules/auth/resources/js/views/Login";
import store from './store.js'

Vue.use(Router);

const routes = [
    {
        path: "/",
        name: "home",
        component: Home,
        meta: { requiresAuth: true }
    },
    {
        path: "/login",
        name: "login",
        component: Login
    },
];

const router = new Router({
    routes: routes,
    linkActiveClass: 'active'
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = store.getters.isAuth;
        if (!auth) {
            next({ name: 'login' })
        } else {
            next()
        }
    } else {
        next()
    }
});

export default router;
