import Vue from "vue";
import Vuex from "vuex";
import auth from '../../app/Modules/auth/resources/js/stores/auth';
import logout from "../../app/Modules/auth/resources/js/stores/logout";

Vue.use(Vuex);

const store =  new Vuex.Store({
    modules: {
        auth,
        logout
    },
    state: {
        apiUrl: 'http://localhost:8000/api',
        serverPath: 'http://localhost:8000',
        token: localStorage.getItem('token'),
        errors: [],
    },
    getters: {
        isAuth: state => {
            return state.token !== "null" && state.token != null
        }
    },
    mutations: {
        SET_TOKEN(state, payload) {
            state.token = payload
        },
        SET_ERRORS(state, payload) {
            state.errors = payload
        },
        CLEAR_ERRORS(state) {
            state.errors = []
        }
    },
});

export default store
