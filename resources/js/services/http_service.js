import store from '../store';
import axios from "axios";

export function http() {
    return axios.create({
        baseURL: store.state.apiUrl,
        headers: {
            // Authorization: "Bearer " + "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU4NTEyNTkyMywiZXhwIjoyMTg1MTI1ODYzLCJuYmYiOjE1ODUxMjU5MjMsImp0aSI6IjlIdjBvSDIyY2N1Wk9rVEsiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.A9UzJm3VDFyUE4Hfcmw45uO3-DJc452TbmgZrZOlJjI"
            // Authorization: "Bearer " + localStorage.getItem("token")
            Authorization: localStorage.getItem('token') !== null ? 'Bearer ' + localStorage.getItem('token') : '',
            'Content-Type': 'application/json'
        }

    });
}

export function httpFile() {
    return axios.create({
        baseURL: store.state.apiUrl,
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}
