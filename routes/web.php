<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('/login', 'Auth\LoginController@login');
//Route::post('/logout', 'Auth\LoginController@logout');
//
//Route::get('/home', 'HomeController@index')->name('home');
//
//Route::group(['middleware' => ['auth', 'roles']], function (){
//    Route::group(['roles' => 'System Administrator'], function (){
//        Route::get('/admin', '\home\Http\Controllers\LoginController@index')->name('admin');
//    });
//});


//Route::get('/{any}', 'FrontEndController@index')->where('any', '.*');

Route::get('/', function () {
    return view('index');
});
