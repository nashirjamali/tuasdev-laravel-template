<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $apiToken = Str::random(40);
            return response()->json(['status' => 'success', 'data' => $apiToken], 200);
        }
        return response()->json(['status' => 'failed']);
    }

    public function logout(Request $request) {
        Auth::logout();
        return response()->json(['status' => 'success']);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    protected function attemptLogin(Request $request)
    {
        $plain = $request->get('password');
        $username = $request->get('username');
        $user = User::all()->where('username', $username)->first();
        $keyZ = $user->keyz;
        $decrypt = hash('sha256', $keyZ . hash('sha256', $plain));

        if (!$user->password_new) {
            if ($decrypt == $user->password) {
                $userNew = User::find($user->userid);
                $userNew->password_new = Hash::make($plain);
                $userNew->save();
                $request['password'] = Hash::make($plain);
            } else {
                return redirect('/login');
            }
        }

        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
}
