<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class CheckUserLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $this->checkRoute($request->route()->getAction());
        if($request->user()->hasRole($roles))
        {
            return $next($request);
        }
        return abort(503, 'Anda tidak memiliki hak akses');
    }

    public function checkRoute($route){
        return isset($route['roles']) ? $route['roles'] : null;
    }
}
