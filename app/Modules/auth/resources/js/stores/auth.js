import * as authService from "../services/auth";

const state = () => ({

});

const mutations = {

};

const actions = {
    submit({ commit }, payload) {
        console.log(payload);
        localStorage.setItem('token', null);
        commit('SET_TOKEN', null, { root: true });
        return new Promise((resolve, reject) => {
            authService.postLogin(payload)
                .then((response) => {
                    console.log(response.data.token);
                    if (response.data.message === 'success') {
                        localStorage.setItem('token', response.data.token);
                        commit('SET_TOKEN', response.data.token, { root: true })
                    } else {
                        commit('SET_ERRORS', { invalid: 'Email/Password Salah' }, { root: true })
                    }
                    resolve(response.data)
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        commit('SET_ERRORS', error.response.data.errors, { root: true })
                    }
                })
        })
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
