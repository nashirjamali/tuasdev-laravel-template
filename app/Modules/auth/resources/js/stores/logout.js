import * as authService from "../services/auth";

const state = () => ({

});

const mutations = {

};

const actions = {
    submit({ commit }) {
        return new Promise((resolve, reject) => {
            authService.postLogout()
                .then((response) => {
                    localStorage.setItem('token', null);
                    commit('SET_TOKEN', null, { root: true });
                    resolve(response.data)
                })
                .catch((error) => {
                    console.log(error)
                    if (error.response.status === 401) {
                        commit('SET_ERRORS', error.response.data.errors, { root: true })
                    }
                })
        })
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
