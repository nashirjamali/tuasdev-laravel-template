import {http, httpFile} from "../../../../../../resources/js/services/http_service";

export function postLogin(data) {
    return http().post('/auth/login', data);
}

export function postLogout() {
    return http().post('/auth/logout');
}
