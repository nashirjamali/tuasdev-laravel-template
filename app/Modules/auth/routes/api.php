<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', '\home\Http\Controllers\LoginController');
    Route::post('logout', '\home\Http\Controllers\LogoutController');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('me', '\home\Http\Controllers\MeController');
    });
});
