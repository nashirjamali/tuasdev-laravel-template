<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'api/auth', 'namespace' => 'Auth'], function () {
    Route::post('login', '\auth\Http\Controllers\LoginController');
    Route::post('logout', '\auth\Http\Controllers\LogoutController');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('me', '\auth\Http\Controllers\MeController');
    });
});


