<?php


namespace auth\Http\Controllers;


use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function __invoke()
    {
        auth()->logout();
    }
}
