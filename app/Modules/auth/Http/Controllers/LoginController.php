<?php

namespace auth\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    public function __invoke(Request $request)
    {
        if(!$token = auth()->attempt($request->only('username', 'password'))) {
            return response(null, 401);
        }
        return response()->json([
            'message' => 'success',
            'token' => $token
        ], 200);
    }

    protected function attemptLogin(Request $request)
    {
        $plain = $request->get('password');
        $username = $request->get('username');
        $user = User::all()->where('username', $username)->first();
        $keyZ = $user->keyz;
        $decrypt = hash('sha256', $keyZ . hash('sha256', $plain));

        if (!$user->password_new) {
            if ($decrypt == $user->password) {
                $userNew = User::find($user->userid);
                $userNew->password_new = Hash::make($plain);
                $userNew->save();
                $request['password'] = Hash::make($plain);
            } else {
                return redirect('/login');
            }
        }

        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

}
