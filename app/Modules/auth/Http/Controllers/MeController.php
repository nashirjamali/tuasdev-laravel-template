<?php

namespace auth\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class MeController extends Controller
{
    public function __invoke(Request $request)
    {
        dd($request->user());
    }
}
