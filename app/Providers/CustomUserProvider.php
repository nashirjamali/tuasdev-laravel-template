<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class CustomUserProvider extends EloquentUserProvider
{
    /**
     * Validate a user against the given credentials.
     *
     * @param UserContract $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password']; // will depend on the name of the input on the login form
        $hashedValue = $user->getAuthPassword();

        $keyZ = $user->keyz;
        $decrypt = hash('sha256', $keyZ . hash('sha256', $plain));
        $hashBcryptPass = null;
        if ($decrypt == $hashedValue) {
            $hashBcryptPass = bcrypt($plain);
        }

        return $this->hasher->check($plain, $hashBcryptPass);
    }
}
