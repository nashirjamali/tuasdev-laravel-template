<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $table = "user_level";
    protected $primaryKey = "id_level";
    protected $fillable = [
        'nama_level', 'keterangan', 'platform', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
